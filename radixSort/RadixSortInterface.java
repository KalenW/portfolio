package radixSort;

/**
 * @author kalen wood-wardlow
 *
 */

/**
 * LeastSignificantDigit Radix Sort:
 * -Take the least significant digit (or group of bits, both being examples of radices) of each integer.
 * -Group the integers based on that digit, but otherwise keep the original order of integers.
 * -Repeat the grouping process with each more significant digit.
 */
public interface RadixSortInterface 
{

    /**
     * This method should re-initialize your RadixSort, clearing all data structures and resetting variables
     */
    void init();

    /**
     * Run a full RadixSort (LSD variant) on the input int array.
     * @param input int array for sorting, modify this input array
     * @param max_significant_digits the maximum amount of digits in any given input in the array
     */
    void sortRadixLSD(int[] input, int max_significant_digits);

    /**
     * Run a single pass of RadixSort, allowing us to step through the process.
     * Checks to the input array after each sorting pass should show increasingly semi-sorted data.
     * @param input the input array (no max_significant_digit is needed as we manually loop).
     */
    void sortRadixLSDOnePass(int[] input);
}
