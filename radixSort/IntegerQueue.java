package radixSort;
import java.util.NoSuchElementException;

/**
 *
 * @author Kalen Wood-Wardlow
 */

/*
 * Queue ADT containing Integer Objects
 * Used in MyRadixSort
 */
public class IntegerQueue 
{
    /*
    @var maxSize: the maximum size of the queue
    @var queue: the generic array that will be the queue
    @var front: points to the front of the queue
    @var rear: points to the rear of the queue
    @var numElem: number of elements in the queue
    */
    public int maxSize;
    public int[] queue;
    public int front;
    public int rear;
    public int numElem;

    
    /*
    Constructor for IntegerQueue
    @param size: declares the max size for the queue
    */
    public IntegerQueue(int size)
    {
        this.maxSize = size;
        this.numElem = 0;
        this.front = 0;
        this.rear = 0;
        final int[] queue = new int[size];
        this.queue = queue;
    }
    
    //adds a value to the end of the queue
    public void add(int element) 
    {
        if(numElem == maxSize) //checks if queue is full then increases size
        {
            this.maxSize += 10;
        }
        queue[rear++] = element;  
        numElem++;
    }

    //removes the value from the front of the queue
    public int remove() throws NoSuchElementException 
    {
       if(numElem == 0) //checks if queue is empty then throws an exception if it is
       {
           throw new NoSuchElementException("Queue is Empty");
       } 
       else
       {
    	   if(front == -1)
    	   {
    		   front = 0;
    	   }
           int temp = front;
           front++;
           if(front == maxSize-1)
           {
               this.front = 0;
           }
           numElem--;
           return queue[temp];  
       }
    }

    //returns the front value without removing it
    public int peek() 
    {
        return queue[front];
    }

    //returns the size of the queue
    public int size() 
    {
        return numElem;
    }

    //returns the maxSize of the queue
    public int getMaxCapacity() 
    {
        return maxSize;
    }
    
    /*
    isEmpty return a boolean value on if the queue is full or not
    @return true if empty, false if not empty
    */
    public Boolean isEmpty()
    {
        if(size() == 0)
        {
            return true;
        }
       
        else
        {
            return false;
        }
    }
}

