package radixSort;
/**
 * 
 */

/**
 * @author kalen wood-wardlow
 *
 */
public class RadixSort implements RadixSortInterface 
{
	/*
	 * @var bin used to sort the array by its digit
	 * @var digit the nth digit, ex: digit = 10; would be 2 in integer 321
	 */
	public IntegerQueue[] bin;
	public int digit;
	
	
	/*
	 * init() resets the variables to initial state
	 */
	public void init() 
	{
		createBin(10);
		digit = 1;
	}

	/*
	 *sortRadixLSD(int[], int) performs radix sort on a intger array
	 *@param input the array to sorted
	 *@param max_significant_digits the longest length of integers(by their digits)
	 */
	public void sortRadixLSD(int[] input, int max_significant_digits) 
	{
		digit = 1;
		int numElem = input.length;
		createBin(numElem);
		
		for(int i = 1; i <= max_significant_digits; i++) // Calls sort on each digit until the max digit
		{												// has been sorted
			sortRadixLSDOnePass(input); //sorts the digit
			createBin(numElem); //Refresh the bin
		}
	}

	/* 
	 * sortRadixLSDOnePass(int[]) sorts the array by the nth digits place
	 * @param input the array to sorted by its nth digit
	 */
	public void sortRadixLSDOnePass(int[] input) 
	{
		if(digit == 0){digit = 1;}
		for(int i = 0; i < input.length; i++) //places the int in the correct bin by its digit
		{
			int index = (input[i] / digit) % 10; //find its digit value for that spot
			bin[index].add(input[i]); //adds the int to its correct bin
		}
		
		int n = 0;
		for(int k = 0; k <= 9; k++) //puts the ints back into the array in the correct (0-9) order
		{
			while(bin[k].isEmpty() != true) //emptys each bin in order back into the array
			{
				input[n++] = bin[k].remove();
			}
		}
		digit *= 10; //increment digit by a 10s spot
	}
	
	/*
	 * createBin() creates/resets the bin to initial state
	 * @param size the size of each queue in the bin
	 */
	public void createBin(int size)
	{
		bin = new IntegerQueue[10]; //creates queues for each digit (0-9)
		
		for(int i = 0; i<10; i++)
		{
			bin[i] = new IntegerQueue(size); //places the queue object in the correct bin spot(by digit)
		}
	}
}
