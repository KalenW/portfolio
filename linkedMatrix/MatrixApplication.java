package linkedMatrix;

/**
 * @author Kalen Wood-Wardlow
 *
 */
public class MatrixApplication {
	
	public static void main(String[] args) 
	{
		MyLinkedMatrix matrix = new MyLinkedMatrix();
		
		matrix.init(6, 6);
		matrix.display();

	}

}
