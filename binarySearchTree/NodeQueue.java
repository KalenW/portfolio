package binarySearchTree;


import java.lang.reflect.Array;
import java.util.NoSuchElementException;

/**
 *
 * @author Kalen Wood-Wardlow
 */
public class NodeQueue extends GenericBST
{
    /*
    @var maxSize: the maximum size of the queue
    @var queue: the array that will be the queue contains Node objects
    @var front: points to the front of the queue
    @var rear: points to the rear of the queue
    @var numElem: number of elements in the queue
    */
    public int maxSize;
    public Node[] queue;
    public int front;
    public int rear;
    public int numElem;

    
    /*
    Constructor for NodeQueue
    @param size: declares the max size for the queue
    */
    
    public NodeQueue(int size)
    {
        this.maxSize = size;
        this.numElem = 0;
        this.front = 0;
        this.rear = -1;
        final Node[] queue = new Node[size];
        this.queue = queue;
    }
    
    /*
     * add() adds a Node to the queue
     * @param element the node to be added
     */
    public void add(Node element) 
    {
        if(numElem==maxSize) //checks if queue is full then increases size
        {
            this.maxSize += 10;
        }
        
        if(rear == maxSize-1)
        {
            rear = -1;
        }
        
        queue[++rear] = element;
        numElem++;
 
    }

    /*
     * removes the front node of the queue
     * @return the node that was removed
     */
    public Node remove() throws NoSuchElementException 
    {
       if(numElem == 0) //checks if queue is empty then throws an exception if it is
       {
           throw new NoSuchElementException("Queue is Empty");
       } 
       else
       {
    	   if(front == -1)
    	   {
    		   front = 0;
    	   }
           int temp = front;
           front++;
           if(front == maxSize-1)
           {
               this.front = 0;
           }
           numElem--;
           return queue[temp];  
       }
    }

    //lets you look at front node w/o removing it
    public Node peek() 
    {
        return queue[front];
    }

    //returns the size of the queue
    public int size() 
    {
        return numElem;
    }

    //returns the maxSize of the queue
    public int getMaxCapacity() 
    {
        return maxSize;
    }
    
 
 
    
 
    
}
