package binarySearchTree;
/**
 * 
 */

/**
 * @author Kalen Wood-Wardlow
 *
 */
public class GenericBST <Key extends Comparable<Key>,Value> implements BinarySearchTree<Key, Value> 
{
	/*
	 * @var root the top most node in the tree (start node)
	 */
	public Node root;
	
	public class Node
	{
		/*
		 * @var keyVal the key for the node object
		 * @var item the data contained in the node
		 * @var leftChild the smaller value connecting to the node
		 * @var rightChild the bigger value connecting to the node
		 */
		public Key keyVal;
		public Value item;
		public Node leftChild;
		public Node rightChild;
		
		public Node(Key key, Value val) //Initializes the node
		{
			keyVal = key;
			item = val;
			leftChild = null;
			rightChild = null;				
		}
	}
	
	/*
	 * 
	 * isEmpty() returns if the tree is empty or not
	 * @return true is tree is empty, false if not empty
	 */
	public boolean isEmpty() 
	{
		if(root == null){return true;}
		else{return false;}
	}

	/*
	 *find() finds the value of the key in the BST
	 *@param key the key of the item your searching for
	 *@return the keys item
	 */
	public Value find(Key key) 
	{
		Node current = root; //Search Node
		
		while(current.keyVal != key)
		{
			if(current.leftChild == null && current.rightChild == null)
			{
				return null;
			}
			int comparison = current.keyVal.compareTo(key);
			if(comparison == 1)
			{
				current = current.leftChild;
			}
			else
			{
				current = current.rightChild;
			}
		}
		return current.item;
	}
	
	/*
	 * insert() inserts a node into the BST according to the constraints of BST
	 * @param key the key that will be used for acessing the node
	 * @param val the data that will be contained in the node
	 * @return the inserted nodes data, or the replaced nodes data
	 */
	public Value insert(Key key, Value val) 
	{
		Node newNode = new Node(key, val);
		if(isEmpty() == true) //If the BST is empty create the root
		{
			root = newNode;
			return root.item;
		}
		else
		{ 
			/*
			 * @var current the "current" node in  search for the next spot
			 * @var parent the parent node of current
			 */
			Node current, parent;
			current = root;
			
			while(true)
			{
				parent = current;
				int comparison = current.keyVal.compareTo(key);
				if(comparison == 1) //Go left
				{
					current = current.leftChild;
					if(current == null) 
					{	//set the new node = to the parent node
						parent.leftChild = newNode;
						return newNode.item;
					}
				}
				else //Go right
				{
					current = current.rightChild;
					if(current == null)
					{	//set the new node = to the parent node
						parent.rightChild = newNode;
						return newNode.item;
					}
				}
				if(comparison == 0) //If keyVal is equal to each other
				{ 	//replace the old node with the new one
					Node temp = current;
					current.item = val;
					return temp.item; //return the old nodes data
				}
			}
		}
	}

	/*
	 *delete() deletes the node corresponding to the key
	 *@param key the keyVal of the node to be deleted
	 *@return the removed nodes data
	 */
	public Value delete(Key key) {
		Node current, parent;
		current = root;
		parent = root;
		String child = "left";
		
		while(current.keyVal != key) //searches for the correct node
		{
			parent = current;
			int comparison = current.keyVal.compareTo(key);
			if(comparison == 1) //Go left
			{
				child = "left";
				current = current.leftChild;
			}
			else //Go right
			{
				child = "right";
				current = current.rightChild;
			}
			if(current == null) //If keyVal is equal to each other
			{
				return null;
			}
		}
		
		if(current.rightChild == null && current.leftChild == null) //if the node is a leaf(no children nodes)
		{														   //delete the node and return it
			if(current == root)
			{
				Node temp = current;
				root = null;
				return temp.item;
			}
			if(child == "right")
			{
				Node temp = current;
				parent.rightChild = null;
				return temp.item;
			}
			else //if the node is a left child
			{
				Node temp = current;
				parent.leftChild = null;
				return temp.item;
			}
		}
		
		if(current.rightChild == null || current.leftChild == null) //if the node has one child node
		{															//replace it with its child node
			if(current.rightChild == null)
			{
				if(current == root)
				{
					Node temp = current;
					root = null;
					return temp.item;
				}
				if(child == "right")
				{
					Node temp = current;
					parent.rightChild = current.leftChild;
					return temp.item;
				}
				else
				{
					Node temp = current;
					parent.leftChild = current.leftChild;
					return temp.item;
				}
			}
			else 
			{
				if(current == root)
				{
					Node temp = current;
					root = null;
					return temp.item;
				}
				if(child == "right")
				{
					Node temp = current;
					parent.rightChild = current.rightChild;
					return temp.item;
				}
				else
				{
					Node temp = current;
					parent.leftChild = current.rightChild;
					return temp.item;
				}
			}
		}
		
		else //if the node has a left and right child node
		{	//find a replacement smaller than the node to deletes
			//right child then replace with node to delete
			
			Node replacement = findReplacement(current); 
			
			if(current == root)
			{
				Node temp = current;
				root = replacement;
				return temp.item;
			}
			if(child == "right")
			{
				Node temp = current;
				parent.rightChild = replacement;
				return temp.item;
			}
			else
			{
				Node temp = current;
				parent.leftChild = replacement;
				return temp.item;
			}
			
		}
	}
	
	/*
	 * findReplacement() finds the replacement node for when the node to delete has two children
	 * @param the node to start the search at(Node to be deleted)
	 * @return the correct replacement node
	 */
	public Node findReplacement(Node search)
	{
		Node replacement = search;
		Node replacementParent = search;
		Node current = search.rightChild;
		
		while(current.leftChild != null)
		{
			replacementParent = replacement;
			replacement = current;
			current = current.leftChild;
		}
		
		if(current != search.rightChild)
		{
			replacementParent.leftChild = replacement.rightChild;
			replacement.rightChild = search.rightChild;
		}
		return replacement;
	}
	
	public String stringLevelOrder() {
		NodeQueue level = new NodeQueue(30);
		Node current = root;
		String str = "{ ";
		
		while(current != null)
		{
			str += current.item.toString();
			level.add(current.leftChild);
			level.add(current.rightChild);
			current = level.remove();
		}
		
		str += " }";
		return str;
	}



	

}
