package simpleSorting;

/**
 *
 * @author kalen wood-wardlow
 */
public class SimpleSorts implements Sorts
{
    /*
    @var array the integer array to be sorted
    @var itemCount the number of elements in the array
    @var smallestElem the smallest element used for selection sort
    @var iterations the number of iterations performed by the sort
    */
    public int[] array;
    public int itemCount;
    public int smallestElem;
    public int iterations;
    
    /*
    bubbleSort is an algorithm that sorts data by bubbling the biggest value to the top
    of the array
    @param input the unsorted arary to be sorted
    */
    
    public int bubbleSort(int[] input) 
    {
        this.array = input;
        this.itemCount = array.length;
        iterations = 0;
        
       for(int i = 0; i<itemCount ;i--)//Outer loop that loops through array untill all ints are moved in the correct position
       {
         for(int k = 0; k<(itemCount - i -1); k++) //Inner loop that moves i to their correct spot in the array
         {    
           if(array[i]>array[k+1])
           {
               swap(i,k+1);
               iterations++;
           }
         }
       }
       return iterations;
    }
    /*
    selectionSort is an algorithm used to sort data by selcting the smallest value and putting it in its sorted spot at the front
    @param input the unsorted array to be sorted
    */
    
    public int selectionSort(int[] input) 
    {
        this.array = input;
        this.itemCount = array.length;
        iterations = 0;
        
        
        for(int i = 0; i<itemCount; i++) //outer loop that iterates through each spot in array
        {
            this.smallestElem = i;
            for(int j = i+1;j<itemCount-1;j++ ) //inner loop that finds the smallest element then
                                                //sets the correct element to be sorted
            {
                if(array[i]>array[j])
                {
                  this.smallestElem = j;
                  iterations++;
                }
            }
            if(smallestElem != i) //once the loop has the smallest element it swaps the current spot and the smallest element
            {
                swap(i,smallestElem);
                iterations++;
            }
        }
 
        return iterations;
    }

    
    public int insertionSort(int[] input) 
    {
       this.array = input;
       this.itemCount = array.length;
       iterations = 0;
       
       for(int i = 0; i<itemCount;i++) //outer loop that iteraties though each element in the array
       {
           /*
           @var j used for the while loop
           @var tempVal stores the value of current element to be sorted
           */
           int j = i;
           int tempVal = array[i];
           
           while(j>0 && array[j-1]>=tempVal) //inner loop that will shift values over if they are bigger
           {                                 //than the element being inserted
               array[j] = array[j-1]; //shifts bigger values over
               j--;
               iterations++;
           }
           array[j] = tempVal;
           iterations++;
       }
       
       return iterations; 
    }
    
    
    /*
    swap used for bubblesort and selection sort
    @param a1 index value that gets swapped with a2
    @param a2 index value that gets swapped with a1
    */
    public void swap(int a1, int a2)
    {
        /*
        @var temp1 allows the value of the first array to be temporaily stored
        @var temp2 allows the value of the second array to be temporaily stored
        */
        int temp1 = array[a1];
        int temp2 = array[a2];
        this.array[a1] = temp2; //the two values are then swapped using the temp variables
        this.array[a2] = temp1;
    }
    
  

    
}
