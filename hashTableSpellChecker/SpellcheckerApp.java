package hashTableSpellChecker;
import java.util.LinkedList;

/**
*
* @author Kalen Wood-Wardlow
*/

public class SpellcheckerApp {
    public static void main(String[] args) {

        double start, time;

        try {
            Spellchecker hashChecker = new Spellchecker(new HashTableDictionary(1000));

            hashChecker.loadLanguage("dictionary.txt");

            //Spell check test file with hash table dictionary and time duration
            start = System.currentTimeMillis();
            LinkedList<String> mispelled = hashChecker.spellcheck("testFile.txt");
            time = System.currentTimeMillis() - start;

            System.out.println("File spell checked using HashTableDictionary in " + time + " ms: " + mispelled.size() + " unknown words");
            System.out.println(mispelled);
            System.out.println("-----");

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
