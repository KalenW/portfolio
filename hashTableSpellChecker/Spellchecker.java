package hashTableSpellChecker;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

/**
*
* @author Kalen Wood-Wardlow
*/

/**
 * A spellchecker class that takes in a dictionary object and uses it to
 * store and validate the spelling of words.
 */
public class Spellchecker {

    private Dictionary dictionary;

    /**
     * Constructor
     *
     * @param dictionary A Dictonary object for storing and validating words
     */
    public Spellchecker(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * Loads the words of a language file into the dictionary
     *
     * @param filename - The name of a text file containing all the words in the language
     * @throws FileNotFoundException
     */
    public void loadLanguage(String filename) throws FileNotFoundException {
        Scanner in = new Scanner(new File(filename));
        String word;

        //Read in each word in the dictionary file
        while(in.hasNextLine()) {
            word = in.nextLine();
            dictionary.addWord( word.toLowerCase().trim() );
        }
    }

    /**
     * Takes in a text file and returns a linked list of all of the unknown (misspelled) words in the file
     *
     * @param filename - The name of the text file to check
     * @return a linked list of unknown words from the given file
     * @throws FileNotFoundException
     */
    public LinkedList<String> spellcheck(String filename) throws FileNotFoundException {

        LinkedList<String> misspelled = new LinkedList<>();
        Scanner in = new Scanner(new File(filename));
        String line;
        String[] words;

        //Check each line of the specified file
        while(in.hasNextLine()) {

            //Grab the words in the current line of text
            line = in.nextLine();
            words = line.split(" ");

            //Spell check each word in the current line
            for(String word : words) {
                word = word.replaceAll("[^a-zA-Z ]", "").toLowerCase().trim();

                //Ignore empty words
                if(word.equals("")) continue;

                //Add unknown words to the misspelled list
                if(dictionary.isValid(word) != true) {
                    misspelled.add(word);
                }
            }
        }

        return misspelled;
    }
}
