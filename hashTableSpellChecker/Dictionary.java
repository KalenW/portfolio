package hashTableSpellChecker;

/**
*
* @author Kalen Wood-Wardlow
*/

/**
 * Defines the methods for a dictionary that stores and looks up words
 */
public interface Dictionary {

    /**
     * Adds a word to the dictionary by adding it into the hash table
     *
     * @param word - A word to add to the dictionary
     */
    public void addWord(String word);

    /**
     * Checks to see if the given word is in the dictionary
     *
     * @param word - The word to check against the dictionary
     * @return true if the word is in the dictionary, false otherwise
     */
    public boolean isValid(String word);

}
