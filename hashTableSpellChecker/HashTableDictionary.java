package hashTableSpellChecker;
import java.lang.reflect.Array;
import java.lang.Character;
import java.util.ArrayList;
import java.util.TreeSet;

/**
*
* @author Kalen Wood-Wardlow
*/

/**
 * An implementation of the Dictionary interface using a hash table
 */
public class HashTableDictionary implements Dictionary 
{

    TreeSet<String>[] hashTable;

    /**
     * Initialize the dictionary be creating the underlying separate chaining hash table.
     *
     * @param tableSize - The size to use in creating the hash table
     */
    public HashTableDictionary(int tableSize) 
    {
        hashTable = (TreeSet<String>[])Array.newInstance(TreeSet.class, tableSize);
        for(int i=0; i<hashTable.length; i++) 
        {
            hashTable[i] = new TreeSet<String>();
        }
    }

    /**
     * Takes in a word and returns a hash value for that word using any
     * hashing algorithm that you would like.
     *
     * @param word - A word for which to calculate a hash value
     * @return the hash value for the given word
     */
    private int hash(String word) 
    {
        int hashNum = 0;

        for(int i = 0; i < word.length(); i++)
        {
        	char val = word.charAt(i);
        	int value = hashLibrary(val);
        	hashNum += (value) * Math.pow(26, i);
        }
        
        hashNum %= hashTable.length;
        return hashNum;
    }
    
    /**
     * hashLibrary() returns the letters numerical value (1-26)
     * @param letter the letter to return its numerical value
     * @return numerical value for the letter
     */
    private int hashLibrary(char letter)
    {
    	int value = 0;
    	
    	switch(letter)
    	{
	    	case 'a':
	    		value = 1;
	    	case 'b':
	    		value = 2;
	    	case 'c':
	    		value = 3;
	    	case 'd':
	    		value = 4;
	    	case 'e':
	    		value = 5;
	    	case 'f':
	    		value = 6;
	    	case 'g':
	    		value = 7;
	    	case 'h':
	    		value = 8;
	    	case 'i':
	    		value = 9;
	    	case 'j':
	    		value = 10;
	    	case 'k':
	    		value = 11;
	    	case 'l':
	    		value = 12;
	    	case 'm':
	    		value = 13;
	    	case 'n':
	    		value = 14;
	    	case 'o':
	    		value = 15;
	    	case 'p':
	    		value = 16;
	    	case 'q':
	    		value = 17;
	    	case 'r':
	    		value = 18;
	    	case 's':
	    		value = 19;
	    	case 't':
	    		value = 20;
	    	case 'u':
	    		value = 21;
	    	case 'v':
	    		value = 22;
	    	case 'w':
	    		value = 23;
	    	case 'x':
	    		value = 24;
	    	case 'y':
	    		value = 25;
	    	case 'z':
	    		value = 26;
    	}
    		
    	return value;
    }

    /**
     * Adds a word to the dictionary by adding it into the hash table
     *
     * @param word - A word to add to the dictionary
     */
    public void addWord(String word) 
    {
        int index = hash(word);
    	hashTable[index].add(word);
    }

    /**
     * Checks to see if the given word is in the dictionary
     *
     * @param word - The word to check against the dictionary
     * @return true if the word is in the dictionary, false otherwise
     */
    public boolean isValid(String word) 
    {
    	int index = hash(word);
    	boolean contains = hashTable[index].contains(word);
        return contains;
    }

}
